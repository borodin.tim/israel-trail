# Israel Trail Tracking App

- Record your progress
- Track statistics e.g.: speed, time spent, average pace, etc...

TODO: 
- Add snapsot tests for components
- Add unit tests
- Add e2e tests with Cypress
- Add Popup for the Markers - start and end
- Make the start/end Markers draggable