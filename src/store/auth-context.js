import React, { useState, useEffect, useCallback } from 'react';

import { ONE_MINUTE_IN_MS } from '../constants/constants';

let logoutTimer;

const AuthContext = React.createContext({
    token: '',
    isLoggedIn: false,
    userId: '',
    login: (token, expirationTime, userId) => { },
    logout: () => { },
});

const calculateRemainingTime = (expirationTime) => {
    const currentTime = new Date().getTime();
    const adjExpirationTime = new Date(expirationTime).getTime();

    const remainingDuration = adjExpirationTime - currentTime;

    return remainingDuration;
};

const retrieveStoredAuthData = () => {
    const storedToken = localStorage.getItem('authToken');
    const storedUserId = localStorage.getItem('userId');
    const storedExpirationTime = localStorage.getItem('expirationTime');

    const remainingTime = calculateRemainingTime(storedExpirationTime);

    if (remainingTime < ONE_MINUTE_IN_MS) {
        localStorage.removeItem('authToken');
        localStorage.removeItem('expirationTime');
        localStorage.removeItem('userId');
        return null;
    }

    return {
        token: storedToken,
        userId: storedUserId,
        remainingTime,
    };
};

const AuthContextProvider = (props) => {
    const authData = retrieveStoredAuthData();
    let initialToken;
    let initialUserId;
    if (authData) {
        initialToken = authData.token;
        initialUserId = authData.userId;
    }
    const [token, setToken] = useState(initialToken);
    const [userId, setUserId] = useState(initialUserId);

    const userIsLoggedIn = !!token;

    const logoutHandler = useCallback(() => {
        setToken(null);
        setUserId(null);
        localStorage.removeItem('authToken');
        localStorage.removeItem('expirationTime');
        localStorage.removeItem('userId');

        if (logoutTimer) {
            clearTimeout(logoutTimer);
        }
    }, []);

    const loginHandler = (token, expirationTime, userId) => {
        setToken(token);
        setUserId(userId);
        localStorage.setItem('authToken', token);
        localStorage.setItem('expirationTime', expirationTime);
        localStorage.setItem('userId', userId);

        const remainingTime = calculateRemainingTime(expirationTime);
        logoutTimer = setTimeout(logoutHandler, remainingTime);
    };

    useEffect(() => {
        if (authData) {
            logoutTimer = setTimeout(logoutHandler, authData.remainingTime);
        }
    }, [authData, logoutHandler]);

    const contextValue = {
        token: token,
        isLoggedIn: userIsLoggedIn,
        userId: userId,
        login: loginHandler,
        logout: logoutHandler,
    };

    return (
        <AuthContext.Provider
            value={contextValue}
        >
            {props.children}
        </AuthContext.Provider>
    );
};

export {
    AuthContext as default,
    AuthContextProvider,
};
