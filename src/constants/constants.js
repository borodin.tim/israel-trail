export const SIGN_IN_WITH_EMAIL_PASSWORD = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=';
export const SIGN_UP_WITH_EMAIL_PASSWORD = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=';
export const CHANGE_PASSWORD = 'https://identitytoolkit.googleapis.com/v1/accounts:update?key=';

export const DEFAULT_LNG = 34.7;
export const DEFAULT_LAT = 31.34;
export const DEFAULT_ZOOM = 6;

export const ONE_SECOND_IN_MS = 1000;
export const ONE_MINUTE_IN_MS = 60000;

export const MIN_PASSWORD_LENGTH = 6;

export const MAP_ZOOM_10 = 10;
export const MAP_ZOOM_9 = 9;
export const MAP_ZOOM_8 = 8;
export const MAP_ZOOM_7 = 7;
export const MAP_ZOOM_6 = 6;
export const MAP_ZOOM_5 = 5;
export const MAP_LAT_LNG_DIFF_02 = 0.2;
export const MAP_LAT_LNG_DIFF_06 = 0.6;
export const MAP_LAT_LNG_DIFF_1 = 1;
export const MAP_LAT_LNG_DIFF_2 = 2;
export const MAP_LAT_LNG_DIFF_3 = 3;

