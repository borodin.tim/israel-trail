import mapboxgl from 'mapbox-gl';
import React, { useEffect, useRef, useState } from 'react';
import 'mapbox-gl/dist/mapbox-gl.css';

// eslint-disable-next-line import/no-webpack-loader-syntax
// mapboxgl.workerClass = require('worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker').default;
// eslint-disable-next-line import/no-webpack-loader-syntax
import MapboxWorker from 'worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker';
import classes from './MapComponent.module.css';
import { DEFAULT_LAT, DEFAULT_LNG, DEFAULT_ZOOM } from '../constants/constants';

mapboxgl.workerClass = MapboxWorker;
mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN;

const MapComponent = props => {
    const defaultLng = props.lng || DEFAULT_LNG;
    const defaultLat = props.lat || DEFAULT_LAT;
    const defaultZoom = props.zoom || DEFAULT_ZOOM;
    const edgePoints = props.edges;

    const mapContainerRef = useRef(null);
    const [lng, setLng] = useState(defaultLng);
    const [lat, setLat] = useState(defaultLat);
    const [zoom, setZoom] = useState(defaultZoom);
    const [edges, setEdges] = useState([]);

    useEffect(() => {
        const map = new mapboxgl.Map({
            container: mapContainerRef.current,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [lng, lat],
            zoom: zoom,
        });

        map.addControl(new mapboxgl.NavigationControl(), 'top-right');

        map.on('move', () => {
            const { lng, lat } = map.getCenter();
            // eslint-disable-next-line no-magic-numbers
            setLng(lng.toFixed(4));
            // eslint-disable-next-line no-magic-numbers
            setLat(lat.toFixed(4));
            setZoom(map.getZoom().toFixed(2));
        });

        map.on('click', (e) => {
            if (props.setStartLocation && props.setEndLocation) {
                if (edges.length === 0) {
                    const point = {
                        lng: e.lngLat.lng,
                        lat: e.lngLat.lat,
                    };
                    new mapboxgl.Marker()
                        .setLngLat([point.lng, point.lat])
                        .addTo(map);
                    // eslint-disable-next-line max-nested-callbacks
                    setEdges(prevState => [...prevState, point]);
                } else if (edges.length === 1) {
                    const point = {
                        lng: e.lngLat.lng,
                        lat: e.lngLat.lat,
                    };
                    new mapboxgl.Marker()
                        .setLngLat([point.lng, point.lat])
                        .addTo(map);
                    // eslint-disable-next-line max-nested-callbacks
                    setEdges(prevState => [...prevState, point]);
                }
            }
        });

        if (edgePoints) {
            new mapboxgl.Marker()
                .setLngLat([edgePoints.startLng, edgePoints.startLat])
                .addTo(map);
            new mapboxgl.Marker()
                .setLngLat([edgePoints.endLng, edgePoints.endLat])
                .addTo(map);
        }

        // map.on('load', () => {
        //     map.addLayer({
        //         id: 'rpd_parks',
        //         type: 'fill',
        //         source: {
        //             type: 'vector',
        //             url: 'mapbox://mapbox.3o7ubwm8'
        //         },
        //         'source-layer': 'RPD_Parks',
        //         layout: {
        //             visibility: 'visible'
        //         },
        //         paint: {
        //             'fill-color': 'rgba(61,153,80,0.55)'
        //         }
        //     });
        // });

        return () => map.remove();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        if (edges.length === 1) {
            props.setStartLocation(edges[0]);
        }
        if (edges.length === 2) {
            props.setEndLocation(edges[1]);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [edges]);

    return (
        <div className={classes['map-container']}>
            <div className={classes.sidebar}>
                Longitude: {parseFloat(lng).toFixed(2)} | Latitude: {parseFloat(lat).toFixed(2)} | Zoom: {zoom}
            </div>
            <div className={classes.map} ref={mapContainerRef} />
        </div>
    );
};

export default MapComponent;
