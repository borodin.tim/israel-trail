import renderer from 'react-test-renderer';

import ProfileForm from '../ProfileForm';

describe('ProfileForm component', () => {
    test('snapshot', () => {
        const tree = renderer.create(<ProfileForm />).toJSON();

        expect(tree).toMatchSnapshot();
    });
});
