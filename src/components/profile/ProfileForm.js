import React, { useContext, useEffect, useRef } from 'react';

import classes from './ProfileForm.module.css';
import { CHANGE_PASSWORD, MIN_PASSWORD_LENGTH, ONE_SECOND_IN_MS } from '../../constants/constants';
import useHttp from '../../hooks/useHttp';
import authContext from '../../store/auth-context';
import Button from '../UI/Button';

const ProfileForm = () => {
    const passwordRef = useRef();
    const { resData, error, isLoading, isSuccessful, sendRequest } = useHttp();
    const authCtx = useContext(authContext);

    useEffect(() => {
        if (error) {
            alert(error);
        }
    }, [error]);

    useEffect(() => {
        if (isSuccessful) {
            const expirationTime = new Date(new Date().getTime() + (+resData.expiresIn * ONE_SECOND_IN_MS));
            authCtx.login(resData.idToken, expirationTime.toISOString(), resData.localId);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isSuccessful]);

    const handleFormSubmit = (event) => {
        event.preventDefault();

        const enteredPassword = passwordRef.current.value;

        // TODO: verify data and change PW
        if (enteredPassword.length === 0) {
            return;
        }

        if (enteredPassword.length < MIN_PASSWORD_LENGTH) {
            // TODO: handle this somehow
        }

        sendRequest(`${CHANGE_PASSWORD}${process.env.REACT_APP_FIREBASE_WEB_API_KEY}`, {
            method: 'POST',
            body: JSON.stringify({
                idToken: authCtx.token,
                password: enteredPassword,
                returnSecureToken: true,
            }),
            headers: { 'Content-Type': 'application/json' },
        });
    };

    return (
        <div>
            {isSuccessful && <p className="centered">Password changed successfully!</p>}
            {!isSuccessful && <form className={classes.form} onSubmit={handleFormSubmit}>
                <h2>Your User Profile</h2>
                <div className={classes['input-group']}>
                    <label htmlFor="password">New Password</label>
                    <input
                        id="password"
                        type="password"
                        placeholder="Enter new password"
                        minLength="6"
                        ref={passwordRef}
                    />
                </div>
                {!isLoading && <Button
                    className={classes.btn}
                    type={'submit'}
                >
                    Change Password
                </Button>}
                {isLoading && <Button
                    // eslint-disable-next-line no-sequences
                    className={classes['btn-loading', 'btn']}
                    type={'submit'}
                >
                    Changing Password...
                </Button>}
            </form>}
        </div>
    );
};

export default ProfileForm;
