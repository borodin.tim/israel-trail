import React, { useContext, useEffect, useRef } from 'react';
import { Link, useHistory } from 'react-router-dom';

import classes from './RegisterForm.module.css';
import { ONE_SECOND_IN_MS, SIGN_UP_WITH_EMAIL_PASSWORD } from '../../constants/constants';
import useHttp from '../../hooks/useHttp';
import authContext from '../../store/auth-context';
import Button from '../UI/Button';

const RegisterForm = props => {
    const emailInputRef = useRef();
    const passwordInputRef = useRef();
    const { resData, error, isLoading, isSuccessful, sendRequest } = useHttp();
    const authCtx = useContext(authContext);
    const history = useHistory();

    useEffect(() => {
        if (error) {
            alert(error);
        }
    }, [error]);

    useEffect(() => {
        if (isSuccessful) {
            const expirationTime = new Date(new Date().getTime() + (+resData.expiresIn * ONE_SECOND_IN_MS));
            authCtx.login(resData.idToken, expirationTime.toISOString(), resData.localId);
            history.replace('/dashboard');
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isSuccessful]);

    const handleFormSubmit = async (event) => {
        event.preventDefault();

        const enteredEmail = emailInputRef.current.value;
        const enteredPassword = passwordInputRef.current.value;

        // TODO: add validation
        if (enteredEmail.length === 0) {
            return;
        }
        if (enteredPassword.length === 0) {
            return;
        }

        sendRequest(`${SIGN_UP_WITH_EMAIL_PASSWORD}${process.env.REACT_APP_FIREBASE_WEB_API_KEY}`, {
            method: 'POST',
            body: JSON.stringify({
                email: enteredEmail,
                password: enteredPassword,
                returnSecureToken: true,
            }),
            headers: { 'Content-Type': 'application/json' },
        });
    };

    return (
        <div>
            {isSuccessful && <p className="centered">Registration completed successfully!</p>}
            {!isSuccessful && <form className={classes.form} onSubmit={handleFormSubmit}>
                <h2>Register</h2>
                <div className={classes['input-group']}>
                    <label htmlFor="email">Email</label>
                    <input
                        id="email"
                        type="email"
                        placeholder="Enter email"
                        autoComplete="false"
                        ref={emailInputRef}
                    />
                </div>
                <div className={classes['input-group']}>
                    <label htmlFor="password">Password</label>
                    <input
                        id="password"
                        type="password"
                        placeholder="Enter password"
                        ref={passwordInputRef}
                    />
                </div>
                {!isLoading && <Button
                    className={classes.btn}
                    type={'submit'}
                >
                    Register
                </Button>}
                {isLoading && <Button
                    // eslint-disable-next-line no-sequences
                    className={classes['btn-loading', 'btn']}
                    type={'submit'}
                >
                    Registering...
                </Button>}
                <p className={classes.link}>
                    <Link to='/login'>Login into an existing account</Link>
                </p>
            </form>}
        </div>
    );
};

export default RegisterForm;
