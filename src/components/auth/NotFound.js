import React from 'react';
import classes from './NotFound.module.css';

const NotFound = props => {
    return (
        <div className={classes.card}>
            <p className="centered">Page not found</p>
        </div>
    );
};

export default NotFound;
