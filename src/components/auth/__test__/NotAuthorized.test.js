import { BrowserRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';

import NotAuthorized from '../NotAuthorized';

describe('NotAuthorized component', () => {
    test('snapshot', () => {
        const tree = renderer.create(<BrowserRouter><NotAuthorized /></BrowserRouter>).toJSON();

        expect(tree).toMatchSnapshot();
    });
});
