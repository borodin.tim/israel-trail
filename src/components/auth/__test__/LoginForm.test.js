import { BrowserRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';

import LoginForm from '../LoginForm';

describe('LoginForm component', () => {
    test('snapshot', () => {
        const tree = renderer.create(<BrowserRouter><LoginForm /></BrowserRouter>).toJSON();

        expect(tree).toMatchSnapshot();
    });
});
