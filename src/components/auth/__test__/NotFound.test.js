import renderer from 'react-test-renderer';

import NotFound from '../NotFound';

describe('NotFound component', () => {
    test('snapshot', () => {
        const tree = renderer.create(<NotFound />).toJSON();

        expect(tree).toMatchSnapshot();
    });
});
