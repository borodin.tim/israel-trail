import { BrowserRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';

import RegisterForm from '../RegisterForm';

describe('RegisterForm component', () => {
    test('snapshot', () => {
        const tree = renderer.create(<BrowserRouter><RegisterForm /></BrowserRouter>).toJSON();

        expect(tree).toMatchSnapshot();
    });
});
