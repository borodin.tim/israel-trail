import React, { useContext, useEffect, useRef } from 'react';
import { Link, useHistory } from 'react-router-dom';

import classes from './LoginForm.module.css';
import { ONE_SECOND_IN_MS, SIGN_IN_WITH_EMAIL_PASSWORD } from '../../constants/constants';
import useHttp from '../../hooks/useHttp';
import authContext from '../../store/auth-context';
import Button from '../UI/Button';

const LoginForm = () => {
    const emailRef = useRef();
    const passwordRef = useRef();
    const { resData, error, isLoading, isSuccessful, sendRequest } = useHttp();
    const authCtx = useContext(authContext);
    const history = useHistory();

    useEffect(() => {
        if (error) {
            alert(error);
        }
    }, [error]);

    useEffect(() => {
        if (isSuccessful) {
            const expirationTime = new Date(new Date().getTime() + (+resData.expiresIn * ONE_SECOND_IN_MS));
            authCtx.login(resData.idToken, expirationTime.toISOString(), resData.localId);
            history.replace('/dashboard');
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isSuccessful]);

    const handleFormSubmit = async (event) => {
        event.preventDefault();

        const enteredEmail = emailRef.current.value;
        const enteredPassword = passwordRef.current.value;

        // TODO: add validation
        if (enteredEmail.length === 0) {
            return;
        }
        if (enteredPassword.length === 0) {
            return;
        }

        sendRequest(`${SIGN_IN_WITH_EMAIL_PASSWORD}${process.env.REACT_APP_FIREBASE_WEB_API_KEY}`, {
            method: 'POST',
            body: JSON.stringify({
                email: enteredEmail,
                password: enteredPassword,
                returnSecureToken: true,
            }),
            headers: { 'Content-Type': 'application/json' },
        });
    };

    return (
        <form className={classes.form} onSubmit={handleFormSubmit}>
            <h2>Login</h2>
            <div className={classes['input-group']}>
                <label htmlFor="email">Email</label>
                <input
                    id="email"
                    type="email"
                    placeholder="Enter email"
                    autoComplete="false"
                    ref={emailRef}
                />
            </div>
            <div className={classes['input-group']}>
                <label htmlFor="password">Password</label>
                <input
                    id="password"
                    type="password"
                    placeholder="Enter password"
                    ref={passwordRef}
                />
            </div>
            {!isLoading && <Button
                className={classes.btn}
                type={'submit'}
            >
                Login
            </Button>}
            {isLoading && <Button
                // eslint-disable-next-line no-sequences
                className={classes['btn-loading', 'btn']}
                type={'submit'}
            >
                Logging in...
            </Button>}
            <p className={classes.link}>
                <Link to='/register'>Create new account</Link>
            </p>
        </form>
    );
};

export default LoginForm;
