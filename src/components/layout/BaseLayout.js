import React, { Fragment } from 'react';

import classes from './BaseLayout.module.css';
import Header from './Header';

const BaseLayout = props => {
    return (
        <Fragment>
            <Header />
            <main className={classes.main}>
                {props.children}
            </main>
        </Fragment>
    );
};

export default BaseLayout;
