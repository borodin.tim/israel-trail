import React, { useContext } from 'react';
import { Link } from 'react-router-dom';

import classes from './Header.module.css';
import authContext from '../../store/auth-context';
import Button from '../UI/Button';

const Header = props => {
    const authCtx = useContext(authContext);

    const logoutHandler = () => {
        authCtx.logout();
    };

    return (
        <header className={classes.header}>
            <h1 className={classes.logo}>
                <Link to='/'>
                    Israel Trail Tracking
                </Link>
            </h1>
            <nav>
                <ul>
                    {!authCtx.isLoggedIn && <li>
                        <Link to='/login'> Login</Link>
                    </li>}
                    {authCtx.isLoggedIn && <li>
                        <Link to='/dashboard'>
                            Dashboard
                        </Link>
                    </li>}
                    {authCtx.isLoggedIn && <li>
                        <Link to='profile'>
                            Profile
                        </Link>
                    </li>}
                    {authCtx.isLoggedIn && <li>
                        <Button
                            type={'button'}
                            className={classes.btn}
                            onClick={logoutHandler}
                        >
                            Logout
                        </Button>
                    </li>}
                </ul>
            </nav>
        </header>
    );
};


export default Header;
