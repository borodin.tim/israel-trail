import renderer from 'react-test-renderer';

import AddNewSectionForm from '../AddNewSectionForm';

describe('AddNewSectionForm component', () => {
    test('snapshot', () => {
        const tree = renderer.create(<AddNewSectionForm />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
