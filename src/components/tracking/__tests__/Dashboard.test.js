import { BrowserRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';

import Dashboard from '../Dashboard';

jest.mock('MapboxWorker');

describe('Dashboard component', () => {
    test('snapshot - no sections', () => {
        // jest.mock("../../../map/MapComponent", () => () => <></>);
        // jest.mock('mapbox-gl/dist/mapbox-gl', () => ({ Map: () => ({}) }));
        const tree = renderer.create(<BrowserRouter><Dashboard /></BrowserRouter>).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
