import { BrowserRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';

import SectionsList from '../SectionsList';

describe('SectionsList component', () => {
    test('snapshot - no sections', () => {
        const tree = renderer.create(<BrowserRouter><SectionsList /></BrowserRouter>).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
