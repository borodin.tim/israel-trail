/* eslint-disable no-magic-numbers */
import { BrowserRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';

import SectionItem from '../SectionItem';

describe('SectionItem component', () => {
    test('snapshot', () => {
        const data = {
            id: 1,
            name: 'Tel-Aviv - Herzliya',
            startTime: new Date(2020, 11, 23, 8, 0, 0).toISOString(),
            endTime: new Date(2020, 11, 23, 16, 0, 0).toISOString(),
            startLocation: '34.81331017220225 32.197319579766415',
            endLocation: '34.80927523164914 32.09759599545163',
            oneWay: true,
            createdAt: new Date(2021, 11, 23, 16, 0, 0).toISOString(),
        };
        const sectionCounter = 1;
        const tree = renderer.create(
            <BrowserRouter>
                <SectionItem key={data.id} data={data} sectionCounter={sectionCounter} />
            </BrowserRouter>).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
