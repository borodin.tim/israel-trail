import React from 'react';

import classes from './SectionItem.module.css';
import {
    MAP_ZOOM_10,
    MAP_ZOOM_9,
    MAP_ZOOM_8,
    MAP_ZOOM_7,
    MAP_ZOOM_6,
    MAP_ZOOM_5,
    MAP_LAT_LNG_DIFF_02,
    MAP_LAT_LNG_DIFF_06,
    MAP_LAT_LNG_DIFF_1,
    MAP_LAT_LNG_DIFF_2,
    MAP_LAT_LNG_DIFF_3,
} from '../../constants/constants';
import MapComponent from '../../map/MapComponent';

const calculateZoom = (startLng, startLat, endLng, endLat) => {
    const lngDiff = startLng - endLng;
    const latDiff = startLat - endLat;
    const totalDiff = lngDiff + latDiff;

    if (totalDiff < MAP_LAT_LNG_DIFF_02) {
        return MAP_ZOOM_10;
    } else if (totalDiff < MAP_LAT_LNG_DIFF_06) {
        return MAP_ZOOM_9;
    } else if (totalDiff < MAP_LAT_LNG_DIFF_1) {
        return MAP_ZOOM_8;
    } else if (totalDiff < MAP_LAT_LNG_DIFF_2) {
        return MAP_ZOOM_7;
    } else if (totalDiff < MAP_LAT_LNG_DIFF_3) {
        return MAP_ZOOM_6;
    }
    return MAP_ZOOM_5;
};

const SectionItem = ({ data: { name, startTime, endTime, startLocation, endLocation, oneWay, createdAt }, sectionCounter }) => {
    const startLng = startLocation.split(' ')[0];
    const startLat = startLocation.split(' ')[1];
    const endLng = endLocation.split(' ')[0];
    const endLat = endLocation.split(' ')[1];

    const middleLng = (parseFloat(startLng) + parseFloat(endLng)) / 2;
    const middleLat = (parseFloat(startLat) + parseFloat(endLat)) / 2;

    const zoom = calculateZoom(startLng, startLat, endLng, endLat);

    const edgePoints = {
        startLng: startLocation.split(' ')[0],
        startLat: startLocation.split(' ')[1],
        endLng: endLocation.split(' ')[0],
        endLat: endLocation.split(' ')[1],
    };

    return (
        <div className={classes.item}>
            <ul>
                <li className={classes.name}>{name}</li>
                <li>
                    <MapComponent
                        edges={edgePoints}
                        lng={middleLng}
                        lat={middleLat}
                        zoom={zoom}
                    />
                </li>
                <li>Start time: {startTime}</li>
                <li>End time: {endTime}</li>
                <li>Start location: {startLocation}</li>
                <li>End location: {endLocation}</li>
                <li>{oneWay ? 'One way' : 'There and back again'}</li>
                <li className={classes['additional-data']}>
                    <div>Section {sectionCounter}</div>
                    {createdAt && <div>Created at {new Date(createdAt).toLocaleString('en-GB', { timeZone: 'UTC' })}</div>}
                </li>
            </ul>
        </div>
    );
};

export default SectionItem;
