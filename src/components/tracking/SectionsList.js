import React, { useContext, useEffect, useState } from 'react';

import SectionItem from './SectionItem';
import classes from './SectionsList.module.css';
import useHttp from '../../hooks/useHttp';
import authContext from '../../store/auth-context';

const SectionsList = () => {
    const authCtx = useContext(authContext);
    const { resData, error, isLoading, sendRequest } = useHttp();
    const [transformedSections, setTransformedSections] = useState([]);

    useEffect(() => {
        if (error) {
            alert(error);
        }
    }, [error]);

    useEffect(() => {
        sendRequest(`${process.env.REACT_APP_FIREBASE_DB_URL}/users/${authCtx.userId}/sections.json`);
    }, [sendRequest, authCtx.userId]);

    useEffect(() => {
        const transformedData = [];
        for (const key in resData) {
            transformedData.push({
                id: key,
                ...resData[key],
            });
            setTransformedSections(transformedData);
        }
    }, [resData]);

    return (
        <div>
            {isLoading && <p className="centered">Loading...</p>}
            {!isLoading && <div className={classes['sections-list']}>
                {transformedSections && transformedSections.map((itemData, idx) =>
                    <SectionItem
                        key={itemData.id}
                        data={itemData}
                        sectionCounter={idx + 1}
                    />)}
                {(!transformedSections || transformedSections.length === 0) &&
                    <p>No trails sections</p>
                }
            </div>
            }
        </div>
    );
};

export default SectionsList;
