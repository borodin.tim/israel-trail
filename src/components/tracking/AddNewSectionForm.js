import React, { useContext, useEffect, useRef, useState } from 'react';

import { useHistory } from 'react-router';
import classes from './AddNewSectionForm.module.css';
import useHttp from '../../hooks/useHttp';
import MapComponent from '../../map/MapComponent';
import authContext from '../../store/auth-context';
import Button from '../UI/Button';

const AddNewSectionForm = () => {
    const nameRef = useRef(null);
    const startTimeRef = useRef(null);
    const endTimeRef = useRef(null);
    const startLocationRef = useRef(null);
    const endLocationRef = useRef(null);
    const oneWayRef = useRef(null);
    const { error, isLoading, isSuccessful, sendRequest } = useHttp();
    const authCtx = useContext(authContext);
    const history = useHistory();
    const [startLocation, setStartLocation] = useState(null);
    const [endLocation, setEndLocation] = useState(null);

    useEffect(() => {
        if (error) {
            alert(error);
        }
    }, [error]);

    useEffect(() => {
        if (isSuccessful) {
            history.replace('/dashboard');
        }
    }, [isSuccessful, history]);

    const handleFormSubmit = (event) => {
        event.preventDefault();

        // TODO: add validation
        const enteredName = nameRef.current.value;
        if (!enteredName) {
            return;
        }

        const reqBody = {
            name: enteredName,
            startTime: startTimeRef.current.value,
            endTime: endTimeRef.current.value,
            startLocation: startLocationRef.current.value,
            endLocation: endLocationRef.current.value,
            oneWay: oneWayRef.current.value,
            createdAt: new Date(),
        };

        sendRequest(`${process.env.REACT_APP_FIREBASE_DB_URL}/users/${authCtx.userId}/sections.json`, {
            method: 'POST',
            body: JSON.stringify(reqBody),
            headers: { 'Content-Type': 'application/json' },
        });
    };

    const startLocationHandler = (point) => {
        setStartLocation(point);
    };

    const endLocationHandler = (point) => {
        setEndLocation(point);
    };

    useEffect(() => {
        if (startLocation) {
            startLocationRef.current.value = `${startLocation.lng} ${startLocation.lat}`;
        }
    }, [startLocation]);

    useEffect(() => {
        if (endLocation) {
            endLocationRef.current.value = `${endLocation.lng} ${endLocation.lat}`;
        }
    }, [endLocation]);

    return (
        <div className={classes['add-new-section-form']}>
            <h2>Add new section</h2>
            <MapComponent
                setStartLocation={startLocationHandler}
                setEndLocation={endLocationHandler}
            />
            <form className={classes.form} onSubmit={handleFormSubmit}>
                <div className={classes['input-group']}>
                    <label htmlFor="name">Name *</label>
                    <input
                        id="name"
                        type="text"
                        autoComplete="off"
                        ref={nameRef}
                        required
                    />
                    <span>* Required field</span>
                </div>
                <div className={classes['input-group']}>
                    <label htmlFor="start-time">Start time</label>
                    <input
                        id="start-time"
                        type="text"
                        autoComplete="false"
                        ref={startTimeRef}
                    />
                </div>
                <div className={classes['input-group']}>
                    <label htmlFor="end-time">End time</label>
                    <input
                        id="end-time"
                        type="text"
                        autoComplete="false"
                        ref={endTimeRef}
                    />
                </div>
                <div className={classes['input-group']}>
                    <label htmlFor="start-location">Start location</label>
                    <input
                        id="start-location"
                        type="text"
                        autoComplete="false"
                        ref={startLocationRef}
                    />
                </div>
                <div className={classes['input-group']}>
                    <label htmlFor="end-location">End location</label>
                    <input
                        id="end-location"
                        type="text"
                        autoComplete="false"
                        ref={endLocationRef}
                    />
                </div>
                <div className={classes['input-group-checkbox']}>
                    <label htmlFor="one-way">One Way</label>
                    <input
                        id="one-way"
                        type="checkbox"
                        ref={oneWayRef}
                    />
                </div>
                <Button
                    className={classes.btn}
                    type={'submit'}
                >
                    {!isLoading && 'Add section'}
                    {isLoading && 'Adding section...'}
                </Button>
            </form>
        </div>
    );
};

export default AddNewSectionForm;
