import React from 'react';
import { Link } from 'react-router-dom';

import classes from './Dashboard.module.css';
import SectionsList from './SectionsList';
import Button from '../UI/Button';

const Dashboard = props => {
    return (
        <div className={classes.dashboard}>
            <h2>Dashboard</h2>
            <Link to='/add-new-section'>
                <Button className={classes.btn}>
                    <span className={classes.symbol}>+</span>
                    <span>Add new section</span>
                </Button>
            </Link>
            <div className={classes['sections-list']}>
                <SectionsList />
            </div>
        </div>
    );
};

export default Dashboard;
