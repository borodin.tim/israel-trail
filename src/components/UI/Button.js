import React from 'react';

import classes from './Button.module.css';

const Button = props => {
    const actualClasses = props.className ? `${props.className} ${classes.button}` : `${classes.button}`;

    return (
        <button
            type={props.type}
            className={actualClasses}
            onClick={props.onClick}
        >
            {props.children}
        </button>
    );
};

export default Button;
