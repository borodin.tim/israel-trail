import React from 'react';

import Dashboard from '../components/tracking/Dashboard';

const DashboardPage = () => {
    return (
        <div>
            <Dashboard />
        </div>
    );
};

export default DashboardPage;
