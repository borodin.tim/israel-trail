import React from 'react';

import ProfileForm from '../components/profile/ProfileForm';

const Profile = () => {
    return (
        <div>
            <ProfileForm />
        </div>
    );
};

export default Profile;
