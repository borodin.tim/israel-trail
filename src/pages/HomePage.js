import React from 'react';

const Home = () => {
    return (
        <div>
            <h2>Welcome to Israel Trail Tracking App</h2>
            <p>Here, you can track your progress on the Trail, observe statistics, and maps</p>
        </div>
    );
};

export default Home;
