import React from 'react';

import AddNewSectionForm from '../components/tracking/AddNewSectionForm';

const AddNewSectionPage = () => {
    return (
        <AddNewSectionForm />
    );
};

export default AddNewSectionPage;
