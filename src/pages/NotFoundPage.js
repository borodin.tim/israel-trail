import React from 'react';

import NotFound from '../components/auth/NotFound';

const NotFoundPage = () => {
    return (
        <NotFound />
    );
};

export default NotFoundPage;
