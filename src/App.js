import React, { Suspense, useContext } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import './App.css';

import BaseLayout from './components/layout/BaseLayout';

// const HomePage = React.lazy(() => import('./pages/HomePage'));
// const LoginPage = React.lazy(() => import('./pages/LoginPage'));
// const ProfilePage = React.lazy(() => import('./pages/ProfilePage'));
// const RegisterPage = React.lazy(() => import('./pages/RegisterPage'));
// const DashboardPage = React.lazy(() => import('./pages/DashboardPage'));
// const AddNewSectionPage = React.lazy(() => import('./pages/AddNewSectionPage'));
// const NotFoundPage = React.lazy(() => import('./pages/NotFoundPage'));

import AddNewSectionPage from './pages/AddNewSectionPage';
import DashboardPage from './pages/DashboardPage';
import HomePage from './pages/HomePage';
import LoginPage from './pages/LoginPage';
import NotFoundPage from './pages/NotFoundPage';
import ProfilePage from './pages/ProfilePage';
import RegisterPage from './pages/RegisterPage';
import authContext from './store/auth-context';

function App() {
  const authCtx = useContext(authContext);

  return (
    <BaseLayout>
      <Suspense fallback={'Loading...'}>
        <Switch>
          <Route path='/' exact>
            <HomePage />
          </Route>
          {!authCtx.isLoggedIn && (
            <Route path='/login'>
              <LoginPage />
            </Route>
          )}
          <Route path='/profile'>
            {authCtx.isLoggedIn && <ProfilePage />}
            {!authCtx.isLoggedIn && <Redirect to='/login' />}
          </Route>
          {!authCtx.isLoggedIn && (
            <Route path="/register">
              <RegisterPage />
            </Route>
          )}
          <Route path='/dashboard'>
            {authCtx.isLoggedIn && <DashboardPage />}
            {!authCtx.isLoggedIn && <Redirect to='/login' />}
          </Route>
          <Route path='/add-new-section'>
            {authCtx.isLoggedIn && <AddNewSectionPage />}
            {!authCtx.isLoggedIn && <Redirect to='/login' />}
          </Route>
          <Route path='*'>
            {/* <Redirect to='/' /> */}
            <NotFoundPage />
          </Route>
        </Switch>
      </Suspense>
    </BaseLayout>
  );
}

export default App;
